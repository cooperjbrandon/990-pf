var https = require('https');
var fs = require('fs');
var mkdirp = require('mkdirp');
var getDirName = require('path').dirname;

const ninenineties = [{
  foundation: 'Bill & Melinda Gates Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201743179349101119_public.xml?_ga=2.129300111.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'Bill & Melinda Gates Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201623169349100822_public.xml?_ga=2.123532552.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'Bill & Melinda Gates Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201503159349101240_public.xml?_ga=2.51304874.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'Gordon and Betty Moore Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201733139349101318_public.xml?_ga=2.167083581.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'Gordon and Betty Moore Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201633189349100408_public.xml?_ga=2.167083581.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'Gordon and Betty Moore Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201513179349102511_public.xml?_ga=2.167083581.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'The Susan Thompson Buffett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201801239349101070_public.xml?_ga=2.52867371.2072275395.1547518839-382930972.1547518838',
  year: 2017
}, {
  foundation: 'The Susan Thompson Buffett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201701299349101195_public.xml?_ga=2.52867371.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'The Susan Thompson Buffett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201621349349101032_public.xml?_ga=2.52867371.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'The Susan Thompson Buffett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201511349349102161_public.xml?_ga=2.123597192.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'Ford Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201743179349102474_public.xml?_ga=2.123597192.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'Ford Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201613139349100776_public.xml?_ga=2.132641037.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'Ford Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201523179349100347_public.xml?_ga=2.132641037.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'Lilly Endowment Inc.',
  url: 'https://s3.amazonaws.com/irs-form-990/201841429349100419_public.xml?_ga=2.132641037.2072275395.1547518839-382930972.1547518838',
  year: 2017
}, {
  foundation: 'Lilly Endowment Inc.',
  url: 'https://s3.amazonaws.com/irs-form-990/201741359349102939_public.xml?_ga=2.132641037.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'Lilly Endowment Inc.',
  url: 'https://s3.amazonaws.com/irs-form-990/201601379349102560_public.xml?_ga=2.90021144.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'Lilly Endowment Inc.',
  url: 'https://s3.amazonaws.com/irs-form-990/201531349349103253_public.xml',
  year: 2014
}, {
  foundation: 'Foundation to Promote Open Society',
  url: 'https://s3.amazonaws.com/irs-form-990/201713189349101901_public.xml?_ga=2.95916319.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'Foundation to Promote Open Society',
  url: 'https://s3.amazonaws.com/irs-form-990/201613199349101416_public.xml?_ga=2.95916319.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'Foundation to Promote Open Society',
  url: 'https://s3.amazonaws.com/irs-form-990/201503179349100345_public.xml?_ga=2.95916319.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'The William and Flora Hewlett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201733079349101008_public.xml?_ga=2.91542809.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'The William and Flora Hewlett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201633079349100928_public.xml?_ga=2.91542809.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'The William and Flora Hewlett Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201543089349100004_public.xml?_ga=2.91542809.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'The Robert Wood Johnson Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201733139349101558_public.xml?_ga=2.59626158.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'The Robert Wood Johnson Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201643099349100224_public.xml?_ga=2.59626158.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'The Robert Wood Johnson Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201513079349100316_public.xml?_ga=2.59626158.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'The David and Lucile Packard Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201733149349101148_public.xml?_ga=2.157269304.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'The David and Lucile Packard Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201643159349100874_public.xml?_ga=2.157269304.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'The David and Lucile Packard Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201513179349101666_public.xml?_ga=2.157269304.2072275395.1547518839-382930972.1547518838',
  year: 2014
}, {
  foundation: 'W.K. Kellogg Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201810509349100356_public.xml?_ga=2.157269304.2072275395.1547518839-382930972.1547518838',
  year: 2017
}, {
  foundation: 'W.K. Kellogg Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201720139349100107_public.xml?_ga=2.84914458.2072275395.1547518839-382930972.1547518838',
  year: 2016
}, {
  foundation: 'W.K. Kellogg Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201630679349100513_public.xml?_ga=2.84914458.2072275395.1547518839-382930972.1547518838',
  year: 2015
}, {
  foundation: 'W.K. Kellogg Foundation',
  url: 'https://s3.amazonaws.com/irs-form-990/201500149349100200_public.xml?_ga=2.84914458.2072275395.1547518839-382930972.1547518838',
  year: 2014
}];

function downloadXml(nineninety) {
  console.log(`downloading 990pf for ${nineninety.foundation} - ${nineninety.year}`);
  return new Promise((resolve, reject) => {
    const path = `xmls/${nineninety.foundation}/${nineninety.year}.xml`
    mkdirp(getDirName(path), function (err) {
      if (err) return reject(err);
      let file = fs.createWriteStream(path);
      let request = https.get(nineninety.url, function(response) {
        response.pipe(file);
        resolve();
      });
    });
  });
}

let go = async function() {

  for (const nineninety of ninenineties) {
    await downloadXml(nineninety);
  }

}

go();