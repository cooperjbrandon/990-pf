const recursive = require('recursive-readdir');
const fs = require('fs');
const xml2js = require('xml2js');
const Json2csvParser = require('json2csv').Parser;
const mkdirp = require('mkdirp');
const getDirName = require('path').dirname;

let createJs = function(file_path) {
  return new Promise((resolve, reject) => {
    var parser = new xml2js.Parser();
    fs.readFile(__dirname + '/' + file_path, function(err, data) {
      if (err) { reject(err); }
      parser.parseString(data, function (err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  });

}

let createCsv = function(flattened) {

  const fields = ['name', 'status', 'description', 'amount'];
  const opts = { fields };
   
  
  return new Promise((resolve, reject) => {

    try {
      const parser = new Json2csvParser(opts);
      const csv = parser.parse(flattened);
      resolve(csv);
    } catch (err) {
      console.error(err);
      reject(err);
    }
  });
}

let saveCsv = function(csv, file_path) {
  return new Promise((resolve, reject) => {
    const desired_path = file_path.replace(new RegExp('xml', 'g'), 'csv');
    mkdirp(getDirName(desired_path), function (err) {
      if (err) return reject(err);
      fs.writeFile(desired_path, csv, function(err) {
        if(err) {
          return console.log(err);
        } else {
          resolve();
        }
      });
    });
  });
}

let formatResult = function(result) {
  const formatted = [];
  const infoGroup = result.Return.ReturnData[0].IRS990PF[0].SupplementaryInformationGrp[0];
  for (const key in infoGroup) {
    if (key === 'GrantOrContributionPdDurYrGrp') {
      const charities = infoGroup[key];

      for (const charity of charities) {
        try {
          formatted.push({
            name: determineName(charity),
            status: charity.RecipientFoundationStatusTxt ? charity.RecipientFoundationStatusTxt[0] : 'N/A',
            description: charity.GrantOrContributionPurposeTxt[0],
            amount: parseInt(charity.Amt[0])
          });
        } catch (err) {
          console.log(err);
          console.log(charity);
          throw err;
        }
      }
    }
  }
  return formatted;
}

let determineName = function(charity) {
  if (charity.RecipientBusinessName) {
    let businessNameObj = charity.RecipientBusinessName[0];
    return businessNameObj.BusinessNameLine1Txt ? businessNameObj.BusinessNameLine1Txt[0] : businessNameObj.BusinessNameLine1[0];
  } else {
    return charity.RecipientPersonNm[0];
  }
}



let go = async function() {
  const file_paths = await recursive('xmls');
  for (const file_path of file_paths) {
    console.log(`Creating CSV for ${file_path}`);
    const result = await createJs(file_path);
    const formatted = formatResult(result);
    const csv = await createCsv(formatted);
    await saveCsv(csv, file_path);
  }
}

go();
